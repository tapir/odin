Odin
====

* Odin is a small component based entity system library for Go.
* See http://godoc.org/github.com/tapir/odin for documentation.

Components
==========

Components are arbitrary Go structs. Although they can be used along with
receivers it's advised to keep the components data-only and do the logic in a 
system.

```go
type Position struct {
	X int
	Y int
}

type Status struct {
	Health float32
	Armor float32
}

playerPosition := odin.NewComponent(&Position{0, 0}, "Position")
playerStatus := odin.NewComponent(&Status{100, 0}, "Status")
```

Entities
========

Entities are a set of components.

```go
player := odin.NewEntity("player")
player.AddComponent(playerPosition)
player.Addcomponent(playerStatus)

enemy := odin.NewEntity("enemy")
enemy.AddComponent(odin.NewComponent(&Position{100, 100}, "Position"))
enemy.AddComponent(odin.NewComponent(&Status{50, 100}, "Status"))
```

Systems
=======

Systems are where you define the logic.

```go
type Render struct {}

func (r *Render) Update(e *Entity, userData interface{}) {
	p := e.GetComponent("Position")
	render(p.X, p.Y)
}

renderSystem := odin.NewSystem(&Render{}, "+Position", odin.PriorityLowest, "render")
```

World
=====
World is where you gather all your entities and systems. World also updates all the
systems and matches the entities with correct systems obeying the matching rules.
If you remove/add component to an entity, updating will also update the entity matches.

```go

newLevel := odin.NewWorld("newLevel")
newLevel.AddEntity(player)
newLevel.AddEntity(enemy)
newLevel.AddSystem(renderSystem)

// Game loop
for {
	newLevel.Update(dt)
}
```
