package odin

// Component corresponds to an arbitrary structure that holds game data.
type Component struct {
	name string
	Data interface{}
}

// NewComponent creates a new component from a given data. Name is used as a key for
// the component list
func NewComponent(data interface{}, name string) *Component {
	return &Component{name, data}
}

// GetName returns the name of the component. Name of the component is its
// unique id.
func (c *Component) GetName() string {
	return c.name
}
