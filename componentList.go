package odin

// For internal use

type componentList map[string](*Component)

func (cmp componentList) add(c *Component) bool {
	_, ok := cmp[c.name]
	if ok {
		return false
	}
	cmp[c.name] = c

	return true
}

func (cmp componentList) remove(name string) bool {
	_, ok := cmp[name]
	if !ok {
		return false
	}
	delete(cmp, name)

	return true
}
