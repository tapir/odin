package odin

import "errors"

// Entity is a set of components.
type Entity struct {
	cList      componentList
	name       string
	isModified bool
}

// NewEntity creates a new entity. Name is used as a key for the entity list.
func NewEntity(name string) *Entity {
	return &Entity{make(componentList), name, false}
}

// AddComponent adds a component to the entity.
func (e *Entity) AddComponent(c *Component) error {
	// add component to entity
	if !e.cList.add(c) {
		return errors.New("Component " + c.name + " already exists in entity " + e.name)
	}

	// flag for modification so that world can refresh matches
	e.isModified = true

	return nil
}

// RemoveComponent removes the component from the entity.
func (e *Entity) RemoveComponent(name string) error {
	// remove the components
	if !e.cList.remove(name) {
		errors.New("Component " + name + " does not exist in entity " + e.name)
	}

	// flag for modification so that world can refresh matches
	e.isModified = true

	return nil
}

// GetComponent returns a pointer to the component specified by name. Returns
// nil if it doesn't exist.
func (e *Entity) GetComponent(name string) *Component {
	return e.cList[name]
}

// GetName returns the name of the entity. Name of the entity is its unique id.
func (e *Entity) GetName() string {
	return e.name
}
