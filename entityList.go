package odin

// For internal use

type entityList map[string](*Entity)

func (en entityList) add(e *Entity) bool {
	_, ok := en[e.name]
	if ok {
		return false
	}
	en[e.name] = e

	return true
}

func (en entityList) remove(name string) bool {
	_, ok := en[name]
	if !ok {
		return false
	}
	delete(en, name)

	return true
}
