package odin

import (
	"errors"
	"strings"
)

// Priority is used for ordering the update priority of the systems.
type Priority uint

const (
	PriorityHighest Priority = iota
	PriorityHigh
	PriorityNormal
	PriorityLow
	PriorityLowest
)

// Systemer consists of the update method that systems should implement.
type Systemer interface {
	// Update method is called everytime the world updates.
	//
	// e is an entitiy that matches with the system.
	//
	// userData is used to pass global game data such as delta time,
	// interpolation etc. to the systems.
	Update(e *Entity, userData interface{})
}

// System is where the logic is processed for matching entities.
type System struct {
	Systemer
	include  []string
	exclude  []string
	one      []string
	priority Priority
	eList    entityList
	name     string
}

// NewSystem creates a new system.
//
// match is used to automatically register matching entites with the system. Example: "+Position +Speed -Boss ~StaticSprite ~AnimatedSprite".
//
// i implements Systemer which consists of an update method.
//
// priority is used to prioritize system update calls.
//
// name is used as a key for the system list.
func NewSystem(i Systemer, match string, priority Priority, name string) (*System, error) {
	var inc, exc, one []string

	// Parse match string
	comps := strings.Split(match, " ")
	for _, cmpStr := range comps {
		l := len(cmpStr)
		if cmpStr[0] == '+' {
			inc = append(inc, cmpStr[1:l])
		} else if cmpStr[0] == '-' {
			exc = append(exc, cmpStr[1:l])
		} else if cmpStr[0] == '~' {
			one = append(one, cmpStr[1:l])
		} else {
			return nil, errors.New("Illegal match string")
		}
	}

	// Check if priority value is correct, if not set to normal
	if priority < PriorityHighest && priority > PriorityLowest {
		priority = PriorityNormal
	}

	return &System{i, inc, exc, one, priority, make(entityList), name}, nil
}

// GetName returns the name of the system. Name of the system is its unique id.
func (s *System) GetName() string {
	return s.name
}

// For internal use
func (s *System) isMatch(e *Entity) bool {
	returnFlag := true

	// check for includes
	for _, incComponent := range s.include {
		_, ok := e.cList[incComponent]
		if !ok {
			returnFlag = returnFlag && false
			break
		}
	}

	// check for excludes
	for _, excComponent := range s.exclude {
		_, ok := e.cList[excComponent]
		if ok {
			returnFlag = returnFlag && false
			break
		}
	}

	// check for one
	for _, oneComponent := range s.one {
		_, ok := e.cList[oneComponent]
		if ok {
			returnFlag = returnFlag && true
			break
		}
	}

	return returnFlag
}
