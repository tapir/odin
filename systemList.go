package odin

// For internal use

type systemList map[string](*System)

func (sys systemList) add(s *System) bool {
	_, ok := sys[s.name]
	if ok {
		return false
	}
	sys[s.name] = s

	return true
}

func (sys systemList) remove(name string) bool {
	_, ok := sys[name]
	if !ok {
		return false
	}
	delete(sys, name)

	return true
}
