package odin

import "errors"

// World holds all the entites and systems.
type World struct {
	eList entityList
	sList systemList
	name  string
	pList [PriorityLowest + 1]([]string)
}

// NewWorld creates a new world.
func NewWorld(name string) *World {
	return &World{eList: make(entityList), sList: make(systemList), name: name}
}

// AddEntity adds an entity to the world and registers it with matching systems.
func (w *World) AddEntity(e *Entity) error {
	// add entity to world entity list
	if !w.eList.add(e) {
		return errors.New("Entity " + e.name + " already exists in world")
	}

	// register to system entity list if matching
	for _, wSystem := range w.sList {
		if wSystem.isMatch(e) {
			wSystem.eList.add(e)
		}
	}

	return nil
}

// RemoveEntity deletes the entity from the world and unregisters it from matching systems.
func (w *World) RemoveEntity(name string) error {
	// delete entity from world entity list
	if !w.eList.remove(name) {
		return errors.New("Entity " + name + " does not exist in the world")
	}

	// unregister entity from system entity lists
	for _, wSystem := range w.sList {
		wSystem.eList.remove(name)
	}

	return nil
}

// AddSystem adds a system to the world.
func (w *World) AddSystem(s *System) error {
	// add system to world system list
	if !w.sList.add(s) {
		return errors.New("System " + s.name + "already exists in world " + w.name)
	}

	// add system priority to world priority list
	w.pList[s.priority] = append(w.pList[s.priority], s.name)

	// register matching entities with this system
	for _, wEntity := range w.eList {
		if s.isMatch(wEntity) {
			s.eList.add(wEntity)
		}
	}

	return nil
}

// RemoveSystem deletes the system from the world.
func (w *World) RemoveSystem(name string) error {
	// check if systeme exists
	_, ok := w.sList[name]
	if !ok {
		return errors.New("System " + name + " does not exist in world " + w.name)
	}

	// purge registered entites in system
	for i, _ := range w.sList[name].eList {
		delete(w.sList[name].eList, i)
	}

	// remove system from world
	w.sList.remove(name)

	// reset world priority list
	var pList [PriorityLowest + 1]([]string)
	for _, wSystem := range w.sList {
		pList[wSystem.priority] = append(pList[wSystem.priority], wSystem.name)
	}
	w.pList = pList

	return nil
}

// Update calls the update function of every system that is added to the world respecting its priorities.
// Update also checks for modified entities and refreshes the matching system entity lists.
//
// userData is used to pass global game data such as delta time,
// interpolation etc. to the systems.
func (w *World) Update(userData interface{}) {
	// refresh matches
	for _, wEntity := range w.eList {
		if wEntity.isModified {
			for _, wSystem := range w.sList {
				// force unregister
				wSystem.eList.remove(wEntity.name)
				// register if matches
				if wSystem.isMatch(wEntity) {
					wSystem.eList.add(wEntity)
				}
			}
			// entity is un/registered correctly
			wEntity.isModified = false
		}
	}

	// run update() method of all systems
	for _, priority := range w.pList {
		if len(priority) > 0 {
			for _, index := range priority {
				system := w.sList[index]
				for _, entity := range system.eList {
					system.Update(entity, userData)
				}
			}
		}
	}
}

// GetEntity returns a pointer to the entity with given name.
// Returns nil if it doesn't exist.
func (w *World) GetEntity(name string) *Entity {
	return w.eList[name]
}

// GetSystem returns a pointer to the system with given name.
// Returns nil if it doesn't exist.
func (w *World) GetSystem(name string) *System {
	return w.sList[name]
}

// GetName returns the name of the world. World names are not unique.
func (w *World) GetName() string {
	return w.name
}
