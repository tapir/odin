package odin

import (
	"testing"
)

// Components
type StaticSprite struct{}
type AnimatedSprite struct{}
type Position struct{}
type Movable struct{}

// Systemers
type Render struct{}

func (r *Render) Update(e *Entity, userData interface{}) {}

type Control struct{}

func (c *Control) Update(e *Entity, userData interface{}) {}

type Animate struct{}

func (a *Animate) Update(e *Entity, userData interface{}) {}

type KillNpc struct{}

func (k *KillNpc) Update(e *Entity, userData interface{}) {}

func TestWorld(t *testing.T) {
	// Create entities
	weapon := NewEntity("weapon")

	err := weapon.AddComponent(NewComponent(&StaticSprite{}, "StaticSprite"))
	if err != nil {
		t.Error(err)
	}

	err = weapon.AddComponent(NewComponent(&Position{}, "Position"))
	if err != nil {
		t.Error(err)
	}

	player := NewEntity("player")

	err = player.AddComponent(NewComponent(&Position{}, "Position"))
	if err != nil {
		t.Error(err)
	}

	err = player.AddComponent(NewComponent(&AnimatedSprite{}, "AnimatedSprite"))
	if err != nil {
		t.Error(err)
	}

	err = player.AddComponent(NewComponent(&Movable{}, "Movable"))
	if err != nil {
		t.Error(err)
	}

	// Create systems
	render, err := NewSystem(&Render{}, "+Position ~AnimatedSprite ~StaticSprite", PriorityLowest, "Render")
	if err != nil {
		t.Error(err)
	}

	control, err := NewSystem(&Control{}, "+Movable", PriorityLow, "Control")
	if err != nil {
		t.Error(err)
	}

	animate, err := NewSystem(&Animate{}, "+AnimatedSprite", PriorityHigh, "Animate")
	if err != nil {
		t.Error(err)
	}

	killnpc, err := NewSystem(&KillNpc{}, "+AnimatedSprite -Movable", PriorityLow, "KillNpc")
	if err != nil {
		t.Error(err)
	}

	// Is match test
	if !render.isMatch(weapon) {
		t.Error("Weapon-Render match test failed.")
	}

	if !render.isMatch(player) {
		t.Error("Player-Render match test failed.")
	}

	if control.isMatch(weapon) {
		t.Error("Weapon-Control match test failed.")
	}

	if !control.isMatch(player) {
		t.Error("Player-control match test failed.")
	}

	if animate.isMatch(weapon) {
		t.Error("Weapon-Animate match test failed.")
	}

	if !animate.isMatch(player) {
		t.Error("Player-animate match test failed")
	}

	if killnpc.isMatch(weapon) {
		t.Error("Weapon-KillNpc match test failed.")
	}

	if killnpc.isMatch(player) {
		t.Error("Player-KillNpc match test failed.")
	}

	// Create world
	world := NewWorld("world")
	world.AddEntity(weapon)
	world.AddEntity(player)
	world.AddSystem(render)
	world.AddSystem(control)
	world.AddSystem(animate)
	world.AddSystem(killnpc)

	_, ok := render.eList["weapon"]
	if !ok {
		t.Error("Render-Weapon failed.")
	}

	_, ok = render.eList["player"]
	if !ok {
		t.Error("Render-Player failed.")
	}

	_, ok = control.eList["weapon"]
	if ok {
		t.Error("Control-Weapon failed.")
	}

	_, ok = control.eList["player"]
	if !ok {
		t.Error("Control-Player failed.")
	}

	_, ok = animate.eList["weapon"]
	if ok {
		t.Error("Animate-Weapon failed.")
	}

	_, ok = animate.eList["player"]
	if !ok {
		t.Error("Animate-Player failed.")
	}

	_, ok = killnpc.eList["weapon"]
	if ok {
		t.Error("KillNpc-Weapon failed.")
	}

	_, ok = killnpc.eList["player"]
	if ok {
		t.Error("KillNpc-Player failed.")
	}

	// Modify an entity
	weapon.RemoveComponent("StaticSprite")
	weapon.AddComponent(NewComponent(&AnimatedSprite{}, "AnimatedSprite"))
	world.Update(3.0)

	_, ok = render.eList["weapon"]
	if !ok {
		t.Error("Render-Weapon failed.")
	}

	_, ok = animate.eList["weapon"]
	if !ok {
		t.Error("Animate-Weapon failed.")
	}
}
